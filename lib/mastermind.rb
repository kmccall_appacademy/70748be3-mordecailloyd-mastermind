require "pry"
class Code
  attr_reader :pegs
  PEGS = {'r'=>'red','g'=>"green",'b'=>"blue",'y'=>"yellow",'o'=>"orange",'p'=>"purple"}
  def initialize(pegs)
    @pegs=pegs
  end

  def ==(compare)
    i=0
    if compare.class != Code
      return false
    end
    while i < 4
      if @pegs[i] != compare[i]
        return false
      end
      i+=1
    end
    true
  end

  def self.random
    i=0
    new_pegs=[]
    while i < 4
      rand_index=rand(0..4)
      new_pegs.push(PEGS.values[rand_index])
      i+=1
    end
    Code.new(new_pegs)
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(compare)
    exact_matches_num=0
    i=0
    while i < 4
      if compare[i] == @pegs[i]
        exact_matches_num +=1
      end
      i+=1
    end
    exact_matches_num
  end

  def near_matches(compare)
    near_matches_num=0
    i=0
    while i < 4
      if @pegs.include?(compare[i]) && @pegs[i] != compare[i]
        near_matches_num+=1
      end
      i+=1
    end
    near_matches_num
  end

  def self.parse(input)
    input=input.downcase.chars
    input.map do |char|
      peg=PEGS[char]
      if peg == nil
        raise 'rawr'
      end
    end
    Code.new (input)
  end




end

class Game
  attr_reader :secret_code
  def initialize(secret=Code.random)
    @secret_code=secret
  end

  def get_guess
    guess=gets
    puts guess
    puts "LOOK HEREEEEEEE"
    return Code.parse(guess)
  end






end
